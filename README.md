# Ankommen-in-Deutschland

Simple HTML/CSS websites of interactive maps for people from Ukraine in order to help them to find help and governmental services in different cities in Germany.


***


## Name
Ankommen-in-Deutschland

## Description
Simple HTML/CSS websites of interactive maps for people from Ukraine in order to help them to find help and governmental services in different cities in Germany.

## Installation
Download and run.

## Usage
Free usage.

## Support
Send me a message via tim.frank.neugebauer@gmail.com

## Roadmap
Add more cities.

## Contributing
Fork, Merge request.

## Authors and acknowledgment
Tim Neugebauer.

## License
GPLv3.

## Project status
Maintaining status quo.
